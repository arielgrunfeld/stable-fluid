# Stable Fluids

Based on:
[Stam, Jos. "Stable fluids." Proceedings of the 26th annual conference on Computer graphics and interactive techniques. 1999](https://dl.acm.org/doi/pdf/10.1145/311535.311548)

---

## Controls

Left-click to add color.
Right-click to stir.
Middle-scroll to change color (up -> red, down -> cyan).

