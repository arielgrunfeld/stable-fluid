from enum import Enum

import numpy as np
import pyamg


class Direction(Enum):
    ALL = 0
    VERTICAL = 1
    HORIZONTAL = 2


class Boundary:

    @staticmethod
    def set(U, dir):
        U[0,:] = -U[1,:] if dir == Direction.VERTICAL else U[1,:]
        U[-1,:] = -U[-2,:] if dir == Direction.VERTICAL else U[-2,:]
        U[:,0] = -U[:,1] if dir == Direction.HORIZONTAL else U[:,1]
        U[:,-1] = -U[:,-2] if dir == Direction.HORIZONTAL else U[:,-2]
        U[0,0] = 0.5 * (U[1,0] + U[0,1])
        U[-1,0] = 0.5 * (U[-2,0] + U[-1,1])
        U[0,-1] = 0.5 * (U[0,-2] + U[1,-1])
        U[-1,-1] = 0.5 * (U[-2,-1] + U[-1,-2])



class Diffusion:

    def __init__(self, dims, dt, viscosity):
        # stencil = np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]]) - dt * viscosity * np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]])
        stencil = np.array([[0, 0, 0], [0, 1, 0], [0, 0, 0]]) - dt * viscosity * np.array([[1, 1, 1], [1, -8, 1], [1, 1, 1]])
        # stencil = np.array([[0, 0, 0, 0, 0], [0, 0, 0, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]) - dt * viscosity * np.array([[0, 0, 1, 0, 0], [0, 1, 2, 1, 0], [1, 2, -16, 2, 1], [0, 1, 2, 1, 0], [0, 0, 1, 0, 0]])
        A = pyamg.gallery.stencil_grid(stencil, dims, dtype=float, format='csr')
        B = np.ones((A.shape[0], 1))
        self.ml = pyamg.smoothed_aggregation_solver(A, B, max_coarse=10)

    def run(self, data):
        height = data.shape[0]
        width = data.shape[1]
        residuals = []
        x = data.reshape((height * width, 1))
        return self.ml.solve(b=x, x0=x, tol=1e-10, residuals=residuals).reshape((height, width))

    def __call__(self, *args, **kwargs):
        return self.run(*args, **kwargs)


class Transport:

    def __init__(self, dt):
        self.dt = dt

    def run(self, U, V, data):

        # track the characteristics backwards in time
        Is = np.arange(data.shape[0])
        Js = np.arange(data.shape[1])
        X0 = np.maximum(1.5, np.minimum(Is - self.dt * U.transpose(), data.shape[0] - 1.5)).transpose()
        Y0 = np.maximum(1.5, np.minimum(Js - self.dt * V, data.shape[1] - 1.5))

        # bilinear interpolation of the scalar value on the characteristic
        I1 = X0.astype(np.uint8)
        J1 = Y0.astype(np.uint8)
        I2 = I1 + 1
        J2 = J1 + 1
        A11 = data[I1, J1]
        A12 = data[I1, J2]
        A21 = data[I2, J1]
        A22 = data[I2, J2]
        A = np.multiply(A11, np.multiply(I2 - X0, J2 - Y0)) + \
            np.multiply(A12, np.multiply(I2 - X0, Y0 - J1)) + \
            np.multiply(A21, np.multiply(X0 - I1, J2 - Y0)) + \
            np.multiply(A22, np.multiply(X0 - I1, Y0 - J1))

        return A

    def __call__(self, *args, **kwargs):
        return self.run(*args, **kwargs)


class Project:

    def __init__(self, dims):
        # stencil = np.array([[0, 1, 0], [1, -4, 1], [0, 1, 0]])
        stencil = np.array([[1, 1, 1], [1, -8, 1], [1, 1, 1]])
        # stencil = np.array([[0, 0, 1, 0, 0], [0, 1, 2, 1, 0], [1, 2, -16, 2, 1], [0, 1, 2, 1, 0], [0, 0, 1, 0, 0]])
        A = pyamg.gallery.stencil_grid(stencil, (dims[0]-2,dims[1]-2), dtype=float, format='csr')
        B = np.ones((A.shape[0], 1))
        self.ml = pyamg.smoothed_aggregation_solver(A, B, max_coarse=10)

    def run(self, U, V):
        div = 0.5 * ((U[2:,1:-1] - U[:-2,1:-1]) + (V[1:-1,2:] - V[1:-1,:-2]))
        height = div.shape[0]
        width = div.shape[1]
        residuals = []
        x = div.reshape((height * width, 1))
        x0 = np.zeros((height * width, 1), dtype=float)
        Q = self.ml.solve(b=x, x0=x0, tol=1e-10, residuals=residuals).reshape((height, width))
        dQU = np.zeros((height+2,width+2), dtype=float)
        dQV = np.zeros((height+2,width+2), dtype=float)
        dQU[2:-2,2:-2] = 0.5 * (Q[2:,1:-1] - Q[:-2,1:-1])
        dQV[2:-2,2:-2] = 0.5 * (Q[1:-1,2:] - Q[1:-1,:-2])
        return U-dQU , V-dQV

    def __call__(self, *args, **kwargs):
        return self.run(*args, **kwargs)



class Data:
    def __init__(self, height, width):
        self.__data = np.zeros([height, width, 5], dtype=float)

    def get(self):
        return self.__data

    def set(self, new_data):
        self.__data = new_data

    def R(self):
        return self.__data[:,:,0]

    def G(self):
        return self.__data[:,:,1]

    def B(self):
        return self.__data[:,:,2]

    def U(self):
        return self.__data[:,:,3]

    def V(self):
        return self.__data[:,:,4]

    def __getitem__(self, k):
        return self.__data.__getitem__(k)

    def __setitem__(self, k, v):
        self.__data.__setitem__(k , v)


class Simulation:

    def __init__(self, height, width, viscosity, dissipation, dt, proj_iter=3):
        self.height = height
        self.width = width
        self.viscosity = viscosity
        self.dissipation = dissipation
        self.dt = dt
        self.proj_iter = proj_iter
        self.data = Data(height, width)

        self.diffuse = Diffusion((height, width), dt, viscosity)
        self.dissipate = Diffusion((height, width), dt, dissipation)
        self.transport = Transport(dt)
        self.project = Project((height, width))

    def update(self):
        odata = self.data
        new_data = np.zeros(odata.get().shape, dtype=float)
        for k in range(3):
            tdata = self.transport(odata.U(), odata.V(), odata[:, :, k])
            ddata = self.dissipate(tdata)
            new_data[:, :, k] = ddata
        for k in range(3,5):
            tdata = self.transport(odata.U(), odata.V(), odata[:, :, k])
            Boundary.set(tdata, k-2)
            ddata = self.diffuse(tdata)
            Boundary.set(ddata, k-2)
            new_data[:, :, k] = ddata
        for _ in range(self.proj_iter):
            new_data[:, :, 3], new_data[:, :, 4] = self.project(new_data[:, :, 3], new_data[:, :, 4])
            Boundary.set(new_data[:, :, 3], Direction.VERTICAL)
            Boundary.set(new_data[:, :, 4], Direction.HORIZONTAL)
        self.data.set(new_data)
