import numpy as np
import pyglet
from pyglet.window import key
from pyglet.window import mouse
# import pyglet.gl as gl

import model


DATA_WIDTH = 64
DATA_HEIGHT = 64

WINDOW_WIDTH = 1024
WINDOW_HEIGHT = 640

DT = 0.01
PROJ_ITER = 5
VISCOSITY = 0.1
DISSIPATION = 2
STIR_POWER = 1e2
MOUSE_RADIUS = 2

IMG_FORMAT = 'RGB'


class Config:
    def __init__(self):
        self.window_width = DATA_WIDTH
        self.window_height = DATA_HEIGHT
        self.color = [1,0,0]


config = Config()

sim = model.Simulation(DATA_HEIGHT, DATA_WIDTH, VISCOSITY, DISSIPATION, DT, PROJ_ITER)

window = pyglet.window.Window(WINDOW_WIDTH, WINDOW_HEIGHT, resizable=True)
window.maximize()

image_data = pyglet.image.ImageData(
        DATA_WIDTH, DATA_HEIGHT, IMG_FORMAT, sim.data.get().tobytes(), DATA_WIDTH * 3
    )


@window.event
def on_key_press(symbol, modifiers):
    if symbol == key.A:
        print('The "A" key was pressed.')
    elif symbol == key.LEFT:
        print('The left arrow key was pressed.')
    elif symbol == key.ENTER:
        print('The enter key was pressed.')


@window.event
def on_mouse_drag(x, y, dx, dy, buttons, modifiers):

    R = MOUSE_RADIUS

    # TODO: for now I'm adding forces through here, might refactor later
    if buttons & (mouse.LEFT | mouse.RIGHT):

        # convert between framebuffer coordinates and data coordinates
        h1 = int(y / config.window_height * DATA_HEIGHT)
        w1 = int(x / config.window_width * DATA_WIDTH)
        dhf = dy / config.window_height * DATA_HEIGHT
        dwf = dx / config.window_width * DATA_WIDTH

        dh = int(dhf)
        dw = int(dwf)
        h0 = h1-dh
        w0 = w1-dw

        # split the path into units of length 1
        phi = np.arctan2(dhf,dwf)
        r = np.sqrt(dhf ** 2 + dwf ** 2)
        dj = np.sin(phi)
        di = np.cos(phi)

        hf = h0
        wf = w0

        # modifying only the current coordinates results in a
        # discontinuous path, so for a smooth path you modify
        # everything between the last recorded coordinates
        # and the new ones
        for t in range(int(np.ceil(r))):
            h = int(np.round(hf))
            w = int(np.round(wf))
            hf = hf + dj
            wf = wf + di
            if buttons & mouse.LEFT:
                # add color
                for k in range(3):
                    sim.data[h-R:h+R,w-R:w+R,k] = config.color[k]
            if buttons & mouse.RIGHT:
                # stir the fluid
                sim.data[h-R:h+R,w-R:w+R,3] = dhf * STIR_POWER
                sim.data[h-R:h+R,w-R:w+R,4] = dwf * STIR_POWER


@window.event
def on_mouse_scroll(x, y, scroll_x, scroll_y):
    # mouse scroll up to move toward red
    # mouse scroll down to move toward cyan
    r = scroll_y/10
    config.color[0] = min(1, max(config.color[0] + r, 0))
    config.color[1] = min(1, max(config.color[1] - r/2, 0))
    config.color[2] = min(1, max(config.color[2] - r/2, 0))


@window.event
def on_resize(width, height):
    config.window_width = width
    config.window_height = height


@window.event
def on_draw():
    window.clear()
    idata = (sim.data.get()[:,:,:3] * 255).astype(np.uint8)
    image_data.set_data(IMG_FORMAT, DATA_WIDTH * 3, idata.tobytes())
    texture = image_data.get_texture()

    # some OpenGL mumbo-jumbo, might be unnecessary
    # gl.glEnable(texture.target)
    # gl.glBindTexture(texture.target, texture.id)

    texture.width = config.window_width
    texture.height = config.window_height

    texture.blit(0,0)


def update(dt):
    sim.update()

if __name__ == "__main__":

    pyglet.clock.schedule_interval(update, DT)

    pyglet.app.run()
